#include "los_config.h"
#include "los_debug.h"
#include "los_interrupt.h"
#include "los_task.h"
#include "los_tick.h"


extern void Balance_task(void *pvParameters);
extern void MPU6050_task(void *pvParameters);
extern void show_task(void *pvParameters);
extern void led_task(void *pvParameters);
extern void pstwo_task(void *pvParameters);
extern void data_task(void *pvParameters);

#define TaskStackSize   1024 * 6

VOID start_app_thread(VOID)
{
    UINT32 uwRet;
    UINT32 taskID1;
    UINT32 taskID2;
	UINT32 taskID3;
    UINT32 taskID4;
    UINT32 taskID5;
    UINT32 taskID6;
    TSK_INIT_PARAM_S stTask1 = {0};
    TSK_INIT_PARAM_S stTask2 = {0};
    TSK_INIT_PARAM_S stTask3 = {0};
    TSK_INIT_PARAM_S stTask4 = {0};
    TSK_INIT_PARAM_S stTask5 = {0};
    TSK_INIT_PARAM_S stTask6 = {0};

    OLED_ShowString(0,10,"TS");
    OLED_Refresh_Gram();

    stTask1.pfnTaskEntry = (TSK_ENTRY_FUNC)Balance_task;
    stTask1.uwStackSize = TaskStackSize;
    stTask1.pcName = "Balance_task";
    stTask1.usTaskPrio = 7; /* Os task priority is 7 */
    uwRet = LOS_TaskCreate(&taskID1, &stTask1);
    if (uwRet != LOS_OK) {
        printf("Balance_task create failed\r\n");

        OLED_ShowString(0,10,"T1 E");
        OLED_Refresh_Gram();
    }
#if 1
    stTask2.pfnTaskEntry = (TSK_ENTRY_FUNC)MPU6050_task;
    stTask2.uwStackSize = TaskStackSize;
    stTask2.pcName = "MPU6050_task";
    stTask2.usTaskPrio = 7; /* Os task priority is 7 */
    uwRet = LOS_TaskCreate(&taskID2, &stTask2);
    if (uwRet != LOS_OK) {
        printf("MPU6050_task create failed\r\n");
        OLED_ShowString(20,0,"T2 E");
        OLED_Refresh_Gram();
    }
#endif
#if 1
    stTask4.pfnTaskEntry = (TSK_ENTRY_FUNC)led_task;
    stTask4.uwStackSize = TaskStackSize;
    stTask4.pcName = "led_task";
    stTask4.usTaskPrio = 4; /* Os task priority is 7 */
    uwRet = LOS_TaskCreate(&taskID4, &stTask4);
    if (uwRet != LOS_OK) {
        printf("led_task create failed\r\n");
        OLED_ShowString(20,10,"T3 E");
        OLED_Refresh_Gram();
    }
#endif
#if 1
    stTask3.pfnTaskEntry = (TSK_ENTRY_FUNC)show_task;
    stTask3.uwStackSize = TaskStackSize;
    stTask3.pcName = "show_task";
    stTask3.usTaskPrio = 7; /* Os task priority is 7 */
    uwRet = LOS_TaskCreate(&taskID3, &stTask3);
    if (uwRet != LOS_OK) {
        printf("show_task create failed\r\n");
    }
#endif

#if 0
    stTask6.pfnTaskEntry = (TSK_ENTRY_FUNC)data_task;
    stTask6.uwStackSize = TaskStackSize;
    stTask6.pcName = "data_task";
    stTask6.usTaskPrio = 7; /* Os task priority is 7 */
    uwRet = LOS_TaskCreate(&taskID6, &stTask6);
    if (uwRet != LOS_OK) {
        printf("data_task create failed\r\n");
        OLED_ShowString(20,20,"T5 E");
        OLED_Refresh_Gram();
    }
#endif
#if 0
    stTask5.pfnTaskEntry = (TSK_ENTRY_FUNC)pstwo_task;
    stTask5.uwStackSize = 8 * 1024;
    stTask5.pcName = "pstwo_task";
    stTask5.usTaskPrio = 7; /* Os task priority is 7 */
    uwRet = LOS_TaskCreate(&taskID5, &stTask5);
    if (uwRet != LOS_OK) {
        printf("pstwo_task create failed\r\n");
        OLED_ShowString(0,20,"T4 E");
        OLED_Refresh_Gram();
    }
#endif
}


void app_main(void)
{
    start_app_thread();
}

