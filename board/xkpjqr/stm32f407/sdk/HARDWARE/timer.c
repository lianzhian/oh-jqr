#include "timer.h"




u8 TIM8CH1_CAPTURE_STA = 0;	
u16 TIM8CH1_CAPTURE_UPVAL;
u16 TIM8CH1_CAPTURE_DOWNVAL;




u8 TIM8CH2_CAPTURE_STA = 0;		
u16 TIM8CH2_CAPTURE_UPVAL;
u16 TIM8CH2_CAPTURE_DOWNVAL;




u8 TIM8CH3_CAPTURE_STA = 0;		
u16 TIM8CH3_CAPTURE_UPVAL;
u16 TIM8CH3_CAPTURE_DOWNVAL;




u8 TIM8CH4_CAPTURE_STA = 0;			
u16 TIM8CH4_CAPTURE_UPVAL;
u16 TIM8CH4_CAPTURE_DOWNVAL;

u32 TIM8_T1;
u32 TIM8_T2;
u32 TIM8_T3;
u32 TIM8_T4;



int Remoter_Ch1=1500,Remoter_Ch2=1500,Remoter_Ch3=1500,Remoter_Ch4=1500;


int L_Remoter_Ch1=1500,L_Remoter_Ch2=1500,L_Remoter_Ch3=1500,L_Remoter_Ch4=1500;  


void TIM8_Cap_Init(u16 arr, u16 psc)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	TIM_ICInitTypeDef TIM_ICInitStructure;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM8,ENABLE);  	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE); 	
		
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9; 
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;	
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP; 
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN; 
	GPIO_Init(GPIOC,&GPIO_InitStructure); 

	GPIO_PinAFConfig(GPIOC,GPIO_PinSource6,GPIO_AF_TIM8); 
	GPIO_PinAFConfig(GPIOC,GPIO_PinSource7,GPIO_AF_TIM8); 
	GPIO_PinAFConfig(GPIOC,GPIO_PinSource8,GPIO_AF_TIM8);
	GPIO_PinAFConfig(GPIOC,GPIO_PinSource9,GPIO_AF_TIM8);

	
	TIM_TimeBaseStructure.TIM_Period = arr; 
	
	TIM_TimeBaseStructure.TIM_Prescaler = psc; 	
	
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; 
	
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up; 
	
	
	TIM_TimeBaseInit(TIM8, &TIM_TimeBaseStructure); 

	
	TIM_ICInitStructure.TIM_Channel = TIM_Channel_1; 
  
	TIM_ICInitStructure.TIM_ICPolarity = TIM_ICPolarity_Rising;	
	TIM_ICInitStructure.TIM_ICSelection = TIM_ICSelection_DirectTI; 
	
	TIM_ICInitStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;	
  
	TIM_ICInitStructure.TIM_ICFilter = 0x0F;	  
	TIM_ICInit(TIM8, &TIM_ICInitStructure);

	
	TIM_ICInitStructure.TIM_Channel = TIM_Channel_2;
	
	TIM_ICInitStructure.TIM_ICPolarity = TIM_ICPolarity_Rising;	
	TIM_ICInitStructure.TIM_ICSelection = TIM_ICSelection_DirectTI; 
	
	TIM_ICInitStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;	  
	TIM_ICInitStructure.TIM_ICFilter = 0x00;	  
	TIM_ICInit(TIM8, &TIM_ICInitStructure);

	
	TIM_ICInitStructure.TIM_Channel = TIM_Channel_3;   
	
	TIM_ICInitStructure.TIM_ICPolarity = TIM_ICPolarity_Rising;
	TIM_ICInitStructure.TIM_ICSelection = TIM_ICSelection_DirectTI; 
	
	TIM_ICInitStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;	  
	
	TIM_ICInitStructure.TIM_ICFilter = 0x00;	  
	TIM_ICInit(TIM8, &TIM_ICInitStructure);

	
	TIM_ICInitStructure.TIM_Channel = TIM_Channel_4; 
	
	TIM_ICInitStructure.TIM_ICPolarity = TIM_ICPolarity_Rising;	
	TIM_ICInitStructure.TIM_ICSelection = TIM_ICSelection_DirectTI; 
	
	TIM_ICInitStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;	   
	
	TIM_ICInitStructure.TIM_ICFilter = 0x00;	  
	TIM_ICInit(TIM8, &TIM_ICInitStructure);

  
	NVIC_InitStructure.NVIC_IRQChannel = TIM8_CC_IRQn; 
  
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;  
	
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2; 
	
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; 
	
	
	NVIC_Init(&NVIC_InitStructure);   
	
  
  
	TIM_ITConfig(TIM8, TIM_IT_CC1|TIM_IT_CC2|TIM_IT_CC3|TIM_IT_CC4,	ENABLE);   
	
	TIM_CtrlPWMOutputs(TIM8,ENABLE); 	
	
	TIM_Cmd(TIM8, ENABLE); 		
}

void TIM8_CC_IRQHandler(void)
{
	
	
	
  if(Remoter_Ch2>1600&&Remote_ON_Flag==0&&Deviation_Count>=CONTROL_DELAY)
  {
		
		
		Remote_ON_Flag=1;
	  APP_ON_Flag=0;
		PS2_ON_Flag=0;
		CAN_ON_Flag=0;
		
		Usart1_ON_Flag=0;
		Usart5_ON_Flag=0;
	}
	



	if ((TIM8CH1_CAPTURE_STA & 0X80) == 0) 			
	{
		if (TIM_GetITStatus(TIM8, TIM_IT_CC1) != RESET) 
		{
			TIM_ClearITPendingBit(TIM8, TIM_IT_CC1); 
			if (TIM8CH1_CAPTURE_STA & 0X40)	
			{
				TIM8CH1_CAPTURE_DOWNVAL = TIM_GetCapture1(TIM8); 
				if (TIM8CH1_CAPTURE_DOWNVAL < TIM8CH1_CAPTURE_UPVAL)
				{
					TIM8_T1 = 9999;
				}
				else
					TIM8_T1 = 0;
				Remoter_Ch1 = TIM8CH1_CAPTURE_DOWNVAL - TIM8CH1_CAPTURE_UPVAL + TIM8_T1;	
				if(abs(Remoter_Ch1-L_Remoter_Ch1)>500) Remoter_Ch1=L_Remoter_Ch1; 
					 L_Remoter_Ch1=Remoter_Ch1;
				
				TIM8CH1_CAPTURE_STA = 0; 
				TIM_OC1PolarityConfig(TIM8, TIM_ICPolarity_Rising); 
			}
			else 
			{
				
				
				TIM8CH1_CAPTURE_UPVAL = TIM_GetCapture1(TIM8); 
				TIM8CH1_CAPTURE_STA |= 0X40; 
				TIM_OC1PolarityConfig(TIM8, TIM_ICPolarity_Falling); 
			}
		}
	}
  
	if ((TIM8CH2_CAPTURE_STA & 0X80) == 0)		
	{
		if (TIM_GetITStatus(TIM8, TIM_IT_CC2) != RESET)	
		{
			TIM_ClearITPendingBit(TIM8, TIM_IT_CC2); 
			if (TIM8CH2_CAPTURE_STA & 0X40)	
			{
				TIM8CH2_CAPTURE_DOWNVAL = TIM_GetCapture2(TIM8); 
				if (TIM8CH2_CAPTURE_DOWNVAL < TIM8CH2_CAPTURE_UPVAL)
				{
					TIM8_T2 = 9999;
				}
				else
					TIM8_T2 = 0;
				Remoter_Ch2 = TIM8CH2_CAPTURE_DOWNVAL - TIM8CH2_CAPTURE_UPVAL + TIM8_T2; 
				if(abs(Remoter_Ch2-L_Remoter_Ch2)>500)Remoter_Ch2=L_Remoter_Ch2; 
				L_Remoter_Ch2=Remoter_Ch2;
				
				TIM8CH2_CAPTURE_STA = 0; 
				TIM_OC2PolarityConfig(TIM8, TIM_ICPolarity_Rising); 
			}
			else 
			{
				
				
				TIM8CH2_CAPTURE_UPVAL = TIM_GetCapture2(TIM8); 
				TIM8CH2_CAPTURE_STA |= 0X40; 
				TIM_OC2PolarityConfig(TIM8, TIM_ICPolarity_Falling); 
			}
		}
	}
  
	if ((TIM8CH3_CAPTURE_STA & 0X80) == 0)			
	{
		if (TIM_GetITStatus(TIM8, TIM_IT_CC3) != RESET)	
		{
			TIM_ClearITPendingBit(TIM8, TIM_IT_CC3); 
			if (TIM8CH3_CAPTURE_STA & 0X40)	
			{
				TIM8CH3_CAPTURE_DOWNVAL = TIM_GetCapture3(TIM8); 
				if (TIM8CH3_CAPTURE_DOWNVAL < TIM8CH3_CAPTURE_UPVAL)
				{
					TIM8_T3 = 9999;
				}
				else
					TIM8_T3 = 0;
				Remoter_Ch3 = TIM8CH3_CAPTURE_DOWNVAL - TIM8CH3_CAPTURE_UPVAL + TIM8_T3; 
				if(abs(Remoter_Ch3-L_Remoter_Ch3)>500)Remoter_Ch3=L_Remoter_Ch3; 
									L_Remoter_Ch3=Remoter_Ch3;
				TIM8CH3_CAPTURE_STA = 0; 
				TIM_OC3PolarityConfig(TIM8, TIM_ICPolarity_Rising); 
			}
			else 
			{
				
				
				TIM8CH3_CAPTURE_UPVAL = TIM_GetCapture3(TIM8); 
				TIM8CH3_CAPTURE_STA |= 0X40; 
				TIM_OC3PolarityConfig(TIM8, TIM_ICPolarity_Falling); 
			}
		}
	}


		
		if ((TIM8CH4_CAPTURE_STA & 0X80) == 0)		
		{
			if (TIM_GetITStatus(TIM8, TIM_IT_CC4) != RESET)	
			{
				TIM_ClearITPendingBit(TIM8, TIM_IT_CC4); 
				if (TIM8CH4_CAPTURE_STA & 0X40)	
				{
					TIM8CH4_CAPTURE_DOWNVAL = TIM_GetCapture4(TIM8); 
					if (TIM8CH4_CAPTURE_DOWNVAL < TIM8CH4_CAPTURE_UPVAL)
					{
						TIM8_T4 = 9999;
					}
					else
						TIM8_T4 = 0;
					Remoter_Ch4 = TIM8CH4_CAPTURE_DOWNVAL - TIM8CH4_CAPTURE_UPVAL + TIM8_T4; 
					if(abs(Remoter_Ch4-L_Remoter_Ch4)>500)Remoter_Ch4=L_Remoter_Ch4; 
					L_Remoter_Ch4=Remoter_Ch4;				
					TIM8CH4_CAPTURE_STA = 0; 
					TIM_OC4PolarityConfig(TIM8, TIM_ICPolarity_Rising); 
				}
				else 
				{
					
				  
					TIM8CH4_CAPTURE_UPVAL = TIM_GetCapture4(TIM8); 
					TIM8CH4_CAPTURE_STA |= 0X40; 
					TIM_OC4PolarityConfig(TIM8, TIM_ICPolarity_Falling); 
				}
			}
		}
}

void TIM8_UP_TIM13_IRQHandler(void) 
{ 
	
	
  TIM8->SR&=~(1<<0);	    
}

void TIM8_SERVO_Init(u16 arr,u16 psc)
{
	GPIO_InitTypeDef GPIO_InitStructure;           
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure; 
	TIM_OCInitTypeDef  TIM_OCInitStructure;        
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM8,ENABLE);  	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE); 	
		
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_AF; 
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6|GPIO_Pin_7|GPIO_Pin_8|GPIO_Pin_9;
  GPIO_InitStructure.GPIO_Speed=GPIO_Speed_100MHz; 	
	GPIO_InitStructure.GPIO_OType=GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd=GPIO_PuPd_UP;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
	
	GPIO_PinAFConfig(GPIOC,GPIO_PinSource6,GPIO_AF_TIM8); 
	GPIO_PinAFConfig(GPIOC,GPIO_PinSource7,GPIO_AF_TIM8); 
	GPIO_PinAFConfig(GPIOC,GPIO_PinSource8,GPIO_AF_TIM8); 
	GPIO_PinAFConfig(GPIOC,GPIO_PinSource9,GPIO_AF_TIM8); 


	
	TIM_TimeBaseStructure.TIM_Period = arr; 
	
	TIM_TimeBaseStructure.TIM_Prescaler = psc; 	
	
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; 
	
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up; 
	
	
	TIM_TimeBaseInit(TIM8, &TIM_TimeBaseStructure); 


	
	
  
 	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1; 
	
	
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable; 
	
	
	TIM_OCInitStructure.TIM_Pulse = 0; 
  
  
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;   
	TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Reset;   
	
	
  
	TIM_OC1Init(TIM8, &TIM_OCInitStructure); 
	TIM_OC2Init(TIM8, &TIM_OCInitStructure); 
	TIM_OC3Init(TIM8, &TIM_OCInitStructure); 
	TIM_OC4Init(TIM8, &TIM_OCInitStructure); 
	
	
	TIM_OC1PreloadConfig(TIM8, TIM_OCPreload_Enable);
	TIM_OC2PreloadConfig(TIM8, TIM_OCPreload_Enable);
	TIM_OC3PreloadConfig(TIM8, TIM_OCPreload_Enable);
	TIM_OC4PreloadConfig(TIM8, TIM_OCPreload_Enable);
	

	TIM_CtrlPWMOutputs(TIM8,ENABLE); 	
	
	TIM_Cmd(TIM8, ENABLE); 		 

  
	
	Servo_PWM1=SERVO_INIT+Moveit_Angle1_init;
	Servo_PWM2=SERVO_INIT+Moveit_Angle2_init;
	Servo_PWM3=SERVO_INIT+Moveit_Angle3_init;
	Servo_PWM4=SERVO_INIT+Moveit_Angle4_init;
}












































































