#ifndef __USRATX_H
#define __USRATX_H 

#include "stdio.h"
#include "sys.h"
#include "system.h"

#define DATA_STK_SIZE   512 
#define DATA_TASK_PRIO  4

#define FRAME_HEADER_CAR 0X7B 
#define FRAME_TAIL_CAR 0X7D 
#define FRAME_HEADER_MOVEIT 0XAA 
#define FRAME_TAIL_MOVEIT 0XBB 
#define SEND_DATA_SIZE    24
#define RECEIVE_DATA_SIZE 11

#define follower 2
#define default_mode 1

typedef struct __Mpu6050_Data_ 
{
	short X_data; 
	short Y_data; 
	short Z_data; 
}Mpu6050_Data;

typedef struct _SEND_DATA_  
{
	unsigned char buffer[SEND_DATA_SIZE];
	struct _Sensor_Str_
	{
		unsigned char Frame_Header; 
		short X_speed;	            
		short Y_speed;              
		short Z_speed;              
		short Power_Voltage;        
		Mpu6050_Data Accelerometer; 
		Mpu6050_Data Gyroscope;     
		unsigned char Frame_Tail;   
	}Sensor_Str;
}SEND_DATA;

typedef struct _RECEIVE_DATA_  
{
	unsigned char buffer[RECEIVE_DATA_SIZE];
	struct _Control_Str_
	{
		unsigned char Frame_Header; 
		float X_speed;	            
		float Y_speed;              
		float Z_speed;              
		unsigned char Frame_Tail;   
	}Control_Str;
}RECEIVE_DATA;


void data_task(void *pvParameters);
void data_transition(void);
void USART1_SEND(void);
void USART3_SEND(void);
void USART5_SEND(void);

void CAN_SEND(void);
void uart1_init(u32 bound);
void uart2_init(u32 bound);
void uart3_init(u32 bound);
void uart5_init(u32 bound);

int USART1_IRQHandler(void);
int USART2_IRQHandler(void);
int USART3_IRQHandler(void);
int UART5_IRQHandler(void);

float Vz_to_Akm_Angle(float Vx, float Vz);
float XYZ_Target_Speed_transition(u8 High,u8 Low);
void usart1_send(u8 data);
void usart2_send(u8 data);
void usart3_send(u8 data);
void usart5_send(u8 data);

u8 Check_Sum(unsigned char Count_Number,unsigned char Mode);


#endif

