#ifndef __MOTOR_H
#define __MOTOR_H

#include "system.h"


#define PWM_PORTA1 GPIOB
#define PWM_PIN_A1 GPIO_Pin_8
#define PWMA1 	  TIM10->CCR1

#define PWM_PORTA2 GPIOB
#define PWM_PIN_A2 GPIO_Pin_9 
#define PWMA2 	  TIM11->CCR1	 



#define PWM_PORTB1 GPIOE
#define PWM_PIN_B1 GPIO_Pin_5 
#define PWMB1 	  TIM9->CCR1	 

#define PWM_PORTB2 GPIOE			 
#define PWM_PIN_B2 GPIO_Pin_6 
#define PWMB2 	  TIM9->CCR2	 




#define PWM_PORTC1 GPIOE			 
#define PWM_PIN_C1 GPIO_Pin_11 
#define PWMC1 	  TIM1->CCR2	 

#define PWM_PORTC2 GPIOE			 
#define PWM_PIN_C2 GPIO_Pin_9 
#define PWMC2 	  TIM1->CCR1	 




#define PWM_PORTD1 GPIOE			 
#define PWM_PIN_D1 GPIO_Pin_14 
#define PWMD1 	  TIM1->CCR4	 

#define PWM_PORTD2 GPIOE			 
#define PWM_PIN_D2 GPIO_Pin_13 
#define PWMD2 	  TIM1->CCR3	 


#define EN     PDin(3)  



#define Servo_PWM4  TIM8->CCR4
#define Servo_PWM3  TIM8->CCR3
#define Servo_PWM2  TIM8->CCR2
#define Servo_PWM1  TIM8->CCR1

#define SERVO_INIT 1500  

void Enable_Pin(void);
void TIM1_PWM_Init(u16 arr,u16 psc);
void TIM9_PWM_Init(u16 arr,u16 psc);
void TIM10_PWM_Init(u16 arr,u16 psc);
void TIM11_PWM_Init(u16 arr,u16 psc);			
	


#endif
