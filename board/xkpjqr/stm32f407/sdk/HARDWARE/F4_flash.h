#ifndef __F4_FLASH_H__
#define __F4_FLASH_H__
#include "sys.h"
#include "system.h"


#define STM32_FLASH_BASE 0x08000000 	
 


#define ADDR_FLASH_SECTOR_0     ((u32)0x08000000) 	
#define ADDR_FLASH_SECTOR_1     ((u32)0x08004000) 	
#define ADDR_FLASH_SECTOR_2     ((u32)0x08008000) 	
#define ADDR_FLASH_SECTOR_3     ((u32)0x0800C000) 	
#define ADDR_FLASH_SECTOR_4     ((u32)0x08010000) 	
#define ADDR_FLASH_SECTOR_5     ((u32)0x08020000) 	
#define ADDR_FLASH_SECTOR_6     ((u32)0x08040000) 	
#define ADDR_FLASH_SECTOR_7     ((u32)0x08060000) 	
#define ADDR_FLASH_SECTOR_8     ((u32)0x08080000) 	
#define ADDR_FLASH_SECTOR_9     ((u32)0x080A0000) 	
#define ADDR_FLASH_SECTOR_10    ((u32)0x080C0000) 	
#define ADDR_FLASH_SECTOR_11    ((u32)0x080E0000) 	

u16 STMFLASH_ReadWord(u16 faddr);		  	
void STMFLASH_Write(u32 WriteAddr,u16 *pBuffer,u32 NumToWrite);		
void STMFLASH_Read(u32 ReadAddr,u16 *pBuffer,u32 NumToRead);   		
void Flash_Read(void);
void Flash_Write(void);
extern short Moveit_Angle1_init,Moveit_Angle2_init,Moveit_Angle3_init,Moveit_Angle4_init; 
extern float Position1,Position2,Position3,Position4; 
extern float Velocity1,Velocity2,Velocity3,Velocity4;     
extern float	Position_KP,Position_KI,Position_KD;  
extern int Servo_init_angle_adjust;

#endif







