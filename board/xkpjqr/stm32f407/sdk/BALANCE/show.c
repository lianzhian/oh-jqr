#include "show.h"
int Voltage_Show;
unsigned char i;          
unsigned char Send_Count; 
extern SEND_DATA Send_Data;
extern int MPU9250ErrorCount, EncoderA_Count, EncoderB_Count, EncoderC_Count, EncoderD_Count; 
extern int MPU9250SensorCountA, MPU9250SensorCountB, MPU9250SensorCountC, MPU9250SensorCountD;
extern int Time_count;

int Buzzer_count=25;
void show_task(void *pvParameters)
{
   //u32 lastWakeTime = getSysTickCnt();
   while(1)
   {	
		int i=0;
		static int LowVoltage_1=0, LowVoltage_2=0;
		static int Servo_adjust_timecount;

		LOS_Msleep(100);
		
		
		if(Time_count<50)Buzzer=1; 
		else if(Time_count>=51 && Time_count<100)Buzzer=0;
		 
		if(LowVoltage_1==1 || LowVoltage_2==1)Buzzer_count=0;
		if(Buzzer_count<5)Buzzer_count++;
		if(Buzzer_count<5)Buzzer=1; 
		else if(Buzzer_count==5)Buzzer=0;
		
		if(Beep_Control_Flag)
		{
			Buzzer = 1;
			Servo_adjust_timecount++;
			if(Servo_adjust_timecount == 10)
			{
				Beep_Control_Flag = 0;
				Buzzer = 0;
				Servo_adjust_timecount = 0;
				OLED_Refresh_Gram(); 
        OLED_Clear();  
		    OLED_Refresh_Gram();
			}
		}
		 
		
		for(i=0;i<10;i++)
		{
			Voltage_All+=Get_battery_volt(); 
		}
		Voltage=Voltage_All/10;
		Voltage_All=0;
		
		if(LowVoltage_1==1)LowVoltage_1++; 
		if(LowVoltage_2==1)LowVoltage_2++; 
		if(Voltage>=12.6f)Voltage=12.6f;
		else if(10<=Voltage && Voltage<10.5f && LowVoltage_1<2)LowVoltage_1++;
		else if(Voltage<10 && LowVoltage_1<2)LowVoltage_2++; 

		APP_Show();	 
	  oled_show(); 

   }
}  


void oled_show(void)
{  
   static int count=0;	 
	 int Car_Mode_Show;
	
			Divisor_Mode=2048/CAR_NUMBER+2;
		 Car_Mode_Show=(int) ((Get_adc_Average(Potentiometer,10))/Divisor_Mode); 
		 if(Car_Mode_Show>2)Car_Mode_Show=2;
		 Voltage_Show=Voltage*100; 
		 count++;
	

	if(Servo_init_angle_adjust == 0)
	{

		
		 
		 {	
			 
			 switch(Car_Mode_Show)
			 {
				case Mec_Car:       OLED_ShowString(0,0,"Mec "); break; 
				case FourWheel_Car: OLED_ShowString(0,0,"4WD "); break; 
				case Tank_Car:      OLED_ShowString(0,0,"Tank"); break; 
			 }
			 
			 if(Car_Mode==Mec_Car)
			 {
				 
				 OLED_ShowString(55,0,"GZ");
				 if( gyro[2]<0)  OLED_ShowString(80,0,"-"),OLED_ShowNumber(90,0,-gyro[2],5,12);
				 else            OLED_ShowString(80,0,"+"),OLED_ShowNumber(90,0, gyro[2],5,12);		
			 }
			 else if(Car_Mode==FourWheel_Car||Car_Mode==Tank_Car)
			 {
				 
				 OLED_ShowString(55,0,"BIAS");
				 if( Deviation_gyro[2]<0)  OLED_ShowString(90,0,"-"),OLED_ShowNumber(100,0,-Deviation_gyro[2],3,12);  
				 else                      OLED_ShowString(90,0,"+"),OLED_ShowNumber(100,0, Deviation_gyro[2],3,12);	
			 }
			 
			 if(Car_Mode==Mec_Car||Car_Mode==FourWheel_Car)
			 {
				
				OLED_ShowString(0,10,"A");
				if( MOTOR_A.Target<0)	OLED_ShowString(15,10,"-"),
															OLED_ShowNumber(20,10,-MOTOR_A.Target*1000,5,12);
				else                 	OLED_ShowString(15,10,"+"),
															OLED_ShowNumber(20,10, MOTOR_A.Target*1000,5,12); 
				
				if( MOTOR_A.Encoder<0)OLED_ShowString(60,10,"-"),
															OLED_ShowNumber(75,10,-MOTOR_A.Encoder*1000,5,12);
				else                 	OLED_ShowString(60,10,"+"),
															OLED_ShowNumber(75,10, MOTOR_A.Encoder*1000,5,12);

			 }
			 else if(Car_Mode==Tank_Car)
			 {
				 
				 OLED_ShowString(00,10,"GYRO_Z:");
				 if( gyro[2]<0)  OLED_ShowString(60,10,"-"),
												 OLED_ShowNumber(75,10,-gyro[2],5,12);
				 else            OLED_ShowString(60,10,"+"),
												 OLED_ShowNumber(75,10, gyro[2],5,12);			
			 }	 
			 
			 if(Car_Mode==Mec_Car||Car_Mode==FourWheel_Car)
			 {
				
				OLED_ShowString(0,20,"B");		
				if( MOTOR_B.Target<0)	OLED_ShowString(15,20,"-"),
															OLED_ShowNumber(20,20,-MOTOR_B.Target*1000,5,12);
				else                 	OLED_ShowString(15,20,"+"),
															OLED_ShowNumber(20,20, MOTOR_B.Target*1000,5,12); 
				
				if( MOTOR_B.Encoder<0)OLED_ShowString(60,20,"-"),
															OLED_ShowNumber(75,20,-MOTOR_B.Encoder*1000,5,12);
				else                 	OLED_ShowString(60,20,"+"),
															OLED_ShowNumber(75,20, MOTOR_B.Encoder*1000,5,12);
				
				OLED_ShowString(0,30,"C");
				if( MOTOR_C.Target<0)	OLED_ShowString(15,30,"-"),
															OLED_ShowNumber(20,30,- MOTOR_C.Target*1000,5,12);
				else                 	OLED_ShowString(15,30,"+"),
															OLED_ShowNumber(20,30,  MOTOR_C.Target*1000,5,12); 
					
				if( MOTOR_C.Encoder<0)OLED_ShowString(60,30,"-"),
															OLED_ShowNumber(75,30,-MOTOR_C.Encoder*1000,5,12);
				else                 	OLED_ShowString(60,30,"+"),
															OLED_ShowNumber(75,30, MOTOR_C.Encoder*1000,5,12);
			 }
			 else if(Car_Mode==Tank_Car)
			 {
				 
				 OLED_ShowString(0,20,"L:");
				 if( MOTOR_A.Target<0)	OLED_ShowString(15,20,"-"),
																OLED_ShowNumber(20,20,-MOTOR_A.Target*1000,5,12);
				 else                 	OLED_ShowString(15,20,"+"),
																OLED_ShowNumber(20,20, MOTOR_A.Target*1000,5,12);  
				 if( MOTOR_A.Encoder<0)	OLED_ShowString(60,20,"-"),
																OLED_ShowNumber(75,20,-MOTOR_A.Encoder*1000,5,12);
				 else                 	OLED_ShowString(60,20,"+"),
																OLED_ShowNumber(75,20, MOTOR_A.Encoder*1000,5,12);
				 
				 OLED_ShowString(0,30,"R:");
				 if( MOTOR_B.Target<0)	OLED_ShowString(15,30,"-"),
																OLED_ShowNumber(20,30,-MOTOR_B.Target*1000,5,12);
				 else                 	OLED_ShowString(15,30,"+"),
																OLED_ShowNumber(20,30,  MOTOR_B.Target*1000,5,12);  
					
				 if( MOTOR_B.Encoder<0)	OLED_ShowString(60,30,"-"),
																OLED_ShowNumber(75,30,-MOTOR_B.Encoder*1000,5,12);
				 else                 	OLED_ShowString(60,30,"+"),
																OLED_ShowNumber(75,30, MOTOR_B.Encoder*1000,5,12);

			 }
			 
			 if(Car_Mode==Mec_Car||Car_Mode==FourWheel_Car)
			 {
					
					OLED_ShowString(0,40,"D");
					if( MOTOR_D.Target<0)	OLED_ShowString(15,40,"-"),
																OLED_ShowNumber(20,40,- MOTOR_D.Target*1000,5,12);
					else                 	OLED_ShowString(15,40,"+"),
																OLED_ShowNumber(20,40,  MOTOR_D.Target*1000,5,12); 			
					if( MOTOR_D.Encoder<0)	OLED_ShowString(60,40,"-"),
																OLED_ShowNumber(75,40,-MOTOR_D.Encoder*1000,5,12);
					else                 	OLED_ShowString(60,40,"+"),
																OLED_ShowNumber(75,40, MOTOR_D.Encoder*1000,5,12);
			 }
			 
			
			 else if(Car_Mode==Tank_Car)
			 {
				 
																 OLED_ShowString(00,40,"MA");
				 if( MOTOR_A.Motor_Pwm<0)OLED_ShowString(20,40,"-"),
																 OLED_ShowNumber(30,40,-MOTOR_A.Motor_Pwm,4,12);
				 else                 	 OLED_ShowString(20,40,"+"),
																 OLED_ShowNumber(30,40, MOTOR_A.Motor_Pwm,4,12); 
																 OLED_ShowString(60,40,"MB");
				 if(MOTOR_B.Motor_Pwm<0) OLED_ShowString(80,40,"-"),
																 OLED_ShowNumber(90,40,-MOTOR_B.Motor_Pwm,4,12);
				 else                 	 OLED_ShowString(80,40,"+"),
																 OLED_ShowNumber(90,40, MOTOR_B.Motor_Pwm,4,12);
			 }
			 
				 
			 
			 if(PS2_ON_Flag==1)         OLED_ShowString(0,50,"PS2  ");
			 else if (APP_ON_Flag==1)   OLED_ShowString(0,50,"APP  ");
			 else if (Remote_ON_Flag==1)OLED_ShowString(0,50,"R-C  ");
			 else if (CAN_ON_Flag==1)   OLED_ShowString(0,50,"CAN  ");
			 else if (Usart1_ON_Flag==1) OLED_ShowString(0,50,"UART1");
			 else if (Usart5_ON_Flag==1) OLED_ShowString(0,50,"UART5");
			 else                       OLED_ShowString(0,50,"ROS  ");
				
			 
			 if(EN==1&&Flag_Stop==0)   OLED_ShowString(45,50,"O N");  
			 else                      OLED_ShowString(45,50,"OFF"); 
				
																	OLED_ShowNumber(75,50,Voltage_Show/100,2,12);
																	OLED_ShowString(88,50,".");
																	OLED_ShowNumber(98,50,Voltage_Show%100,2,12);
																	OLED_ShowString(110,50,"V");
			 if(Voltage_Show%100<10) 	OLED_ShowNumber(92,50,0,2,12);
			}
		 

	}
	else  
	{
					
		OLED_ShowString(0,0,"Angle1_init:");
		if( Moveit_Angle1_init<0)	
		{
			OLED_ShowString(100,0,"-");
			OLED_ShowNumber(110,0, -Moveit_Angle1_init,3,12);
		}
		else
		{
			OLED_ShowString(100,00,"+");
			OLED_ShowNumber(110,0, Moveit_Angle1_init,3,12);
		}
							
		OLED_ShowString(0,10,"Angle2_init:");
		if( Moveit_Angle2_init<0)	
		{
			OLED_ShowString(100,10,"-");
			OLED_ShowNumber(110,10, -Moveit_Angle2_init,3,12);
		}
		else
		{
			OLED_ShowString(100,10,"+");
			OLED_ShowNumber(110,10, Moveit_Angle2_init,3,12);
		}
					
		OLED_ShowString(0,20,"Angle3_init:");
		if( Moveit_Angle3_init<0)	
		{
			OLED_ShowString(100,20,"-");
			OLED_ShowNumber(110,20, -Moveit_Angle3_init,3,12);
		}
		else
		{
			OLED_ShowString(100,20,"+");
			OLED_ShowNumber(110,20, Moveit_Angle3_init,3,12);
		}
							
		OLED_ShowString(0,30,"Angle4_init:");
		if( Moveit_Angle4_init<0)	
		{
			OLED_ShowString(100,30,"-");
			OLED_ShowNumber(110,30, -Moveit_Angle4_init,3,12);
		}
		else
		{
			OLED_ShowString(100,30,"+");
			OLED_ShowNumber(110,30, Moveit_Angle4_init,3,12);
		}

	}
			
		OLED_Refresh_Gram();	
} 

void APP_Show(void)
{    
	 static u8 flag_show;
	 int Left_Figure,Right_Figure,Voltage_Show;
	
	 
	 Voltage_Show=(Voltage*1000-10000)/27;
	 if(Voltage_Show>100)Voltage_Show=100; 
	
	 
	 Left_Figure=MOTOR_A.Encoder*100;  if(Left_Figure<0)Left_Figure=-Left_Figure;	
	 Right_Figure=MOTOR_B.Encoder*100; if(Right_Figure<0)Right_Figure=-Right_Figure;
	
	 
	 flag_show=!flag_show;
	
	 if(PID_Send==1) 
	 {
		 
		 		printf("{C%d:%d:%d:%d:%d:%d:%d}$",
		  (int)((Moveit_Angle1+1.57f)*100), 
			(int)((Moveit_Angle2+1.57f)*100),
			(int)((Moveit_Angle3+1.57f)*100),
			(int)((Moveit_Angle4+1.57f)*100),
		  (int)RC_Velocity,
			(int)Velocity_KP,
			(int)Velocity_KI);
		  PID_Send=0;	
	 }	
	 else	if(flag_show==0) 
	 {
		 
		 printf("{A%d:%d:%d:%d}$",(u8)Left_Figure,(u8)Right_Figure,Voltage_Show,(int)gyro[2]);
	 }
	 else
	 {

		 printf("{B%d:%d:%d}$",(int)gyro[0],(int)gyro[1],(int)gyro[2]);
	 }
}


