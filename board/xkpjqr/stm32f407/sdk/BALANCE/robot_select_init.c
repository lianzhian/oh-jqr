#include "robot_select_init.h"



Robot_Parament_InitTypeDef  Robot_Parament; 

void Robot_Select(void)
{
	
  
	Divisor_Mode=2048/CAR_NUMBER+2;
	Car_Mode=(int) ((Get_adc_Average(Potentiometer,10))/Divisor_Mode); 
  if(Car_Mode>2)Car_Mode=2;
	
	switch(Car_Mode)
	{
		case Mec_Car:       Robot_Init(MEC_wheelspacing,         MEC_axlespacing,          0,                     HALL_60F, Photoelectric_500, Mecanum_75);            break; 
		case FourWheel_Car: Robot_Init(Four_Mortor_wheelSpacing, Four_Mortor__axlespacing, 0,                     HALL_60F, Photoelectric_500, Black_WheelDiameter);   break; 
		case Tank_Car:      Robot_Init(Tank_wheelSpacing,        0,                        0,                     HALL_60F, Photoelectric_500, Tank_WheelDiameter);    break; 
	}
	 
	
	switch(Car_Mode)
  {
	 case Mec_Car:       CheckPhrase1=8, CheckPhrase2=14; break; 
	 case FourWheel_Car: CheckPhrase1=8, CheckPhrase2=11; break; 
	 case Tank_Car:      CheckPhrase1=4, CheckPhrase2=7;  break; 
  }
}


void Robot_Init(double wheelspacing, float axlespacing, float omni_turn_radiaus, float gearratio,float Accuracy,float tyre_diameter) 
{
	
	
  Robot_Parament.WheelSpacing=wheelspacing; 
	
  
  Robot_Parament.AxleSpacing=axlespacing;   
	
  
  Robot_Parament.OmniTurnRadiaus=omni_turn_radiaus; 
	
	
  Robot_Parament.GearRatio=gearratio; 
	
  
  Robot_Parament.EncoderAccuracy=Accuracy;
	
  
  Robot_Parament.WheelDiameter=tyre_diameter;       
	
	
	
	Encoder_precision=EncoderMultiples*Robot_Parament.EncoderAccuracy*Robot_Parament.GearRatio;
	
  
	Wheel_perimeter=Robot_Parament.WheelDiameter*PI;
	
  
  Wheel_spacing=Robot_Parament.WheelSpacing; 
  
  
	Axle_spacing=Robot_Parament.AxleSpacing; 
	
  
	Omni_turn_radiaus=Robot_Parament.OmniTurnRadiaus; 
}


