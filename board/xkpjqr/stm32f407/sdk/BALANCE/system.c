

#include "system.h"



u8 Flag_Stop=1;   



int Divisor_Mode;




u8 Car_Mode=0; 



int Servo;  



float RC_Velocity=500;



u8 car_A_steer_flag=1;



float Move_X, Move_Y, Move_Z;   



float Velocity_KP=300,Velocity_KI=300; 



Smooth_Control smooth_control;  



Motor_parameter MOTOR_A,MOTOR_B,MOTOR_C,MOTOR_D;  




float Encoder_precision; 


float Wheel_perimeter; 


float Wheel_spacing; 


float Axle_spacing; 


float Omni_turn_radiaus; 





u8 PS2_ON_Flag=0, APP_ON_Flag=0, Remote_ON_Flag=0, CAN_ON_Flag=0, Usart1_ON_Flag, Usart5_ON_Flag; 


float Moveit_Angle1=0,Moveit_Angle2=-1.57,Moveit_Angle3=1.57,Moveit_Angle4=0;

int  Moveit_PWM1,Moveit_PWM2,Moveit_PWM3,Moveit_PWM4;



u8 Flag_Left, Flag_Right, Flag_Direction=0, Turn_Flag; 



u8 PID_Send; 



int PS2_LX,PS2_LY,PS2_RX,PS2_RY,PS2_KEY; 



int Check=0, Checking=0, Checked=0, CheckCount=0, CheckPhrase1=0, CheckPhrase2=0; 



long int ErrorCode=0; 

void systemInit(void)
{       
	


	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);



	delay_init(168);	
	
	
	
	LED_Init();                     
	    
  
  
	Buzzer_Init();  
	
	
	
	Enable_Pin();

  
  
	OLED_Init();     
	
	
	
	KEY_Init();	
	
	
	
	
	uart1_init(115200);	  
	
	
	
	
	uart2_init(9600);  
	
	
	
	
	uart3_init(115200);
	
	
	
	
	uart5_init(115200);

	
	
	
 	Adc_Init();  
	Adc_POWER_Init();
	
	
  
	CAN1_Mode_Init(1,7,6,3,0); 
	
  
  
  
	Robot_Select();                 
	
   
	
	 
	
	 
  
	 Encoder_Init_TIM2();
	
  
	  Encoder_Init_TIM3();   
	
  
	  Encoder_Init_TIM4(); 
	
	
		Encoder_Init_TIM5(); 
	
	  Flash_Read();
	
	 TIM8_SERVO_Init(9999,168-1);

  
  
  
		TIM1_PWM_Init(16799,0);
		TIM9_PWM_Init(16799,0);
		TIM10_PWM_Init(16799,0);
		TIM11_PWM_Init(16799,0);
		
  
  
	I2C_GPIOInit();

  
	
  
   MPU6050_initialize();        		
	
	
	
	PS2_Init();
	
	
  
	PS2_SetInit();		 							
}
