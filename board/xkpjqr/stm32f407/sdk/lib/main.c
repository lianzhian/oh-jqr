#include "los_config.h"
#include "los_debug.h"
#include "los_interrupt.h"
#include "los_task.h"
#include "los_tick.h"

VOID TaskSample(VOID);

typedef struct
{
  uint32_t id[3];
}ChipID;
 
ChipID Get_ChipID(void)
{
  ChipID chipid = {0};
  
  chipid.id[0] = *(__I uint32_t *)(0x1FFF7A10 + 0x00);
  chipid.id[1] = *(__I uint32_t *)(0x1FFF7A10 + 0x04);
  chipid.id[2] = *(__I uint32_t *)(0x1FFF7A10 + 0x08);
  
  return chipid;
}

unsigned int cod[3];

#define FLASH_COD_ADDR		0x0800FFF0		//???????��


/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
	UINT32 ret;
    ChipID chipid;

	SCB->VTOR = FLASH_BASE | 0x10000;

    chipid = Get_ChipID();
	
	printf("ID:%4x %4x %4x\r\n", chipid.id[0], chipid.id[1], chipid.id[2]);

    cod[0] = *(__I uint32_t *)(FLASH_COD_ADDR + 0x00);
    cod[1] = *(__I uint32_t *)(FLASH_COD_ADDR + 0x04);
    cod[2] = *(__I uint32_t *)(FLASH_COD_ADDR + 0x08);

    printf("cod:%4x %4x %4x\r\n", cod[0], cod[1], cod[2]);

    if( ((chipid.id[0] * 541 + 882377) == cod[0]) && ((chipid.id[1] * 541 + 104729) == cod[1]) && ((chipid.id[2] * 541 + 7919) == cod[2])) 
    {
        printf("cod ok!!!!\r\n");
    }else{
        printf("cod error!!!!\r\n");

        while(1);
    }
	//USART1_UART_Init();
	//ϵͳ��ʼ��
    systemInit();
	
    ret = LOS_KernelInit();

    printf("Open Harmony 3.2 start ...\r\n\r\n");


    if (ret == LOS_OK) {
#if (LOSCFG_USE_SHELL == 1)
        LosShellInit();
        OsShellInit();
#endif
		
        app_main();
        
        LOS_Start();
    }
	
	while (1)
	{

	}
  /* USER CODE END 3 */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
